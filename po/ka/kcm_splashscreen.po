# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-30 00:49+0000\n"
"PO-Revision-Date: 2022-09-02 07:38+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: kcm.cpp:160
#, kde-format
msgid "None"
msgstr "არაფერი"

#: kcm.cpp:162
#, kde-format
msgid "No splash screen will be shown"
msgstr "არ აჩვენო მისასალმებელი ფანჯარა"

#: kcm.cpp:180
#, kde-format
msgid "You cannot delete the currently selected splash screen"
msgstr "ამჟამად არჩეული მისასალმებელი ეკრანის წაშლა შეუძლებელია"

#: package/contents/ui/main.qml:16
#, kde-format
msgid "This module lets you choose the splash screen theme."
msgstr "ეს მოდული მისასალმებელი ეკრანის თემის არჩევის საშუალებას გაძლევთ."

#: package/contents/ui/main.qml:30
#, kde-format
msgid "&Get New…"
msgstr ""

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Failed to show the splash screen preview."
msgstr "მისასალმებელი ეკრანის გადახედვის ჩვენების შეცდომა."

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Preview Splash Screen"
msgstr "მისასალმებელი ეკრანის გადახედვა"

#: package/contents/ui/main.qml:88
#, kde-format
msgid "Uninstall"
msgstr "ამოშლა"

#. i18n: ectx: label, entry (engine), group (KSplash)
#: splashscreensettings.kcfg:12
#, kde-format
msgid "For future use"
msgstr "მომავლისთვის"

#. i18n: ectx: label, entry (theme), group (KSplash)
#: splashscreensettings.kcfg:16
#, kde-format
msgid "Name of the current splash theme"
msgstr "მისასალმებელი ფანჯრის მიმდინარე თემის სახელი"

#~ msgid "&Get New Splash Screens…"
#~ msgstr "&ახალი მისასალმებელი ეკრანების მიღება…"
