# translation of kcmsmserver.po to Macedonian
#
# Copyright (C) 2002,2003, 2004, 2005, 2008, 2009 Free Software Foundation, Inc.
#
# Novica Nakov <novica@bagra.net.mk>, 2003.
# Bozidar Proevski <bobibobi@freemail.com.mk>, 2004, 2005, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmsmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-14 00:48+0000\n"
"PO-Revision-Date: 2009-06-21 13:52+0200\n"
"Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>\n"
"Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : "
"2;\n"

#: kcmsmserver.cpp:49
#, kde-format
msgid ""
"<h1>Session Manager</h1> You can configure the session manager here. This "
"includes options such as whether or not the session exit (logout) should be "
"confirmed, whether the session should be restored again when logging in and "
"whether the computer should be automatically shut down after session exit by "
"default."
msgstr ""
"<h1>Менаџер на сесии</h1> Тука може да го конфигурирате менаџерот на сесии. "
"Ова вклучува опции како што се дали треба напуштањето на сесијата "
"(одјавување) да биде потврдено, дали сесијата треба да биде обновена при "
"повторно најавување и дали компјутерот треба да биде автоматски изгасен по "
"напуштањето на сесијата."

#: package/contents/ui/main.qml:25
#, kde-format
msgid "Failed to request restart to firmware setup: %1"
msgstr ""

#: package/contents/ui/main.qml:26
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the UEFI setup screen."
msgstr ""

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the firmware setup screen."
msgstr ""

#: package/contents/ui/main.qml:32
#, fuzzy, kde-format
#| msgid "&Restart computer"
msgid "Restart Now"
msgstr "&Рестартирај го компјутерот"

#: package/contents/ui/main.qml:38
#, fuzzy, kde-format
#| msgid "General"
msgid "General:"
msgstr "Општо"

#: package/contents/ui/main.qml:39
#, fuzzy, kde-format
#| msgid "Conf&irm logout"
msgctxt "@option:check"
msgid "Confirm logout"
msgstr "Пот&врди го одјавувањето"

#: package/contents/ui/main.qml:48
#, fuzzy, kde-format
#| msgid "O&ffer shutdown options"
msgctxt "@option:check"
msgid "Offer shutdown options"
msgstr "П&онуди опции за гасење"

#: package/contents/ui/main.qml:65
#, fuzzy, kde-format
#| msgid "Default Leave Option"
msgid "Default leave option:"
msgstr "Стандардна опција за гасење"

#: package/contents/ui/main.qml:66
#, fuzzy, kde-format
#| msgid "&End current session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "End current session"
msgstr "&Заврши со активната сесија"

#: package/contents/ui/main.qml:76
#, fuzzy, kde-format
#| msgid "&Restart computer"
msgctxt "@option:radio"
msgid "Restart computer"
msgstr "&Рестартирај го компјутерот"

#: package/contents/ui/main.qml:86
#, fuzzy, kde-format
#| msgid "&Turn off computer"
msgctxt "@option:radio"
msgid "Turn off computer"
msgstr "&Исклучи го компјутерот"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "When logging in:"
msgstr ""

#: package/contents/ui/main.qml:103
#, fuzzy, kde-format
#| msgid "Restore &manually saved session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last session"
msgstr "Обнови ја &рачно зачуваната сесија"

#: package/contents/ui/main.qml:113
#, fuzzy, kde-format
#| msgid "Restore &manually saved session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last manually saved session"
msgstr "Обнови ја &рачно зачуваната сесија"

#: package/contents/ui/main.qml:123
#, fuzzy, kde-format
#| msgid "Start with an empty &session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Start with an empty session"
msgstr "Започни со празна с&есија"

#: package/contents/ui/main.qml:135
#, kde-format
msgid "Don't restore these applications:"
msgstr ""

#: package/contents/ui/main.qml:152
#, kde-format
msgid ""
"Here you can enter a colon or comma separated list of applications that "
"should not be saved in sessions, and therefore will not be started when "
"restoring a session. For example 'xterm:konsole' or 'xterm,konsole'."
msgstr ""
"Тука може да внесете листа на апликации, одвоени со две точки или со "
"запирка, што не треба да се зачувуваат во сесиите, и според тоа нема да се "
"стартуваат при обновување на сесијата. На пример „xterm:xconsole“ или „xterm,"
"xconsole“."

#: package/contents/ui/main.qml:161
#, kde-format
msgctxt "@option:check"
msgid "Enter UEFI setup screen on next restart"
msgstr ""

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "@option:check"
msgid "Enter firmware setup screen on next restart"
msgstr ""

#. i18n: ectx: label, entry (confirmLogout), group (General)
#: smserversettings.kcfg:9
#, fuzzy, kde-format
#| msgid "Conf&irm logout"
msgid "Confirm logout"
msgstr "Пот&врди го одјавувањето"

#. i18n: ectx: label, entry (offerShutdown), group (General)
#: smserversettings.kcfg:13
#, fuzzy, kde-format
#| msgid "O&ffer shutdown options"
msgid "Offer shutdown options"
msgstr "П&онуди опции за гасење"

#. i18n: ectx: label, entry (shutdownType), group (General)
#: smserversettings.kcfg:17
#, fuzzy, kde-format
#| msgid "Default Leave Option"
msgid "Default leave option"
msgstr "Стандардна опција за гасење"

#. i18n: ectx: label, entry (loginMode), group (General)
#: smserversettings.kcfg:26
#, fuzzy, kde-format
#| msgid "On Login"
msgid "On login"
msgstr "При најава"

#. i18n: ectx: label, entry (excludeApps), group (General)
#: smserversettings.kcfg:30
#, fuzzy, kde-format
#| msgid "Applications to be e&xcluded from sessions:"
msgid "Applications to be excluded from session"
msgstr "Апликации кои ќе бидат изоставени од сесиите:"

#, fuzzy
#~| msgid "Restore &previous session"
#~ msgid "Desktop Session"
#~ msgstr "Обнови ја &претходната сесија"

#, fuzzy
#~| msgid "Restore &manually saved session"
#~ msgid "Restore previous saved session"
#~ msgstr "Обнови ја &рачно зачуваната сесија"

#~ msgid ""
#~ "Check this option if you want the session manager to display a logout "
#~ "confirmation dialog box."
#~ msgstr ""
#~ "Изберете ја оваа опција ако сакате менаџерот на сесии да прикажува "
#~ "дијалог за потврда при одјавување."

#~ msgid "Conf&irm logout"
#~ msgstr "Пот&врди го одјавувањето"

#~ msgid "O&ffer shutdown options"
#~ msgstr "П&онуди опции за гасење"

#~ msgid ""
#~ "Here you can choose what should happen by default when you log out. This "
#~ "only has meaning, if you logged in through KDM."
#~ msgstr ""
#~ "Тука може да изберете што треба вообичаено да се случи кога ќе се "
#~ "одјавите. Ова има значење само ако сте се најавиле преку KDM."

#~ msgid "Default Leave Option"
#~ msgstr "Стандардна опција за гасење"

#~ msgid ""
#~ "<ul>\n"
#~ "<li><b>Restore previous session:</b> Will save all applications running "
#~ "on exit and restore them when they next start up</li>\n"
#~ "<li><b>Restore manually saved session: </b> Allows the session to be "
#~ "saved at any time via \"Save Session\" in the K-Menu. This means the "
#~ "currently started applications will reappear when they next start up.</"
#~ "li>\n"
#~ "<li><b>Start with an empty session:</b> Do not save anything. Will come "
#~ "up with an empty desktop on next start.</li>\n"
#~ "</ul>"
#~ msgstr ""
#~ "<ul>\n"
#~ "<li><b>Обнови ја претходната сесија:</b> На излегување ќе ги сними сите "
#~ "апликации што работат и ќе ги врати при следното стартување</li>\n"
#~ "<li><b>Обнови ја рачно зачуваната сесија: </b> Дозволува сесијата во "
#~ "секое време да биде рачно зачувана преку „Зачувај сесија“ во K менито. "
#~ "Тоа значи дека моментално стартуваните апликации ќе се појават при "
#~ "следното стартување.</li>\n"
#~ "<li><b>Започни со празна сесија:</b> Не зачувува ништо. Ќе се стартува со "
#~ "празна работна површина при следното стартување.</li>\n"
#~ "</ul>"

#~ msgid "On Login"
#~ msgstr "При најава"

#~ msgid "Applications to be e&xcluded from sessions:"
#~ msgstr "Апликации кои ќе бидат изоставени од сесиите:"

#~ msgid "Session Manager"
#~ msgstr "Менаџер на сесии"
