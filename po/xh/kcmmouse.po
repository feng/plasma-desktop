# translation of kcminput.po to
# translation of kcminput.po to
# translation of kcminput.po to
# translation of kcminput.po to
# translation of kcminput.po to Xhosa
# K Desktop Environment - kdebase
# Copyright (C) 2001 translate.org.za
# Antoinette Dekeni <antoinette@transalate.org.za>, 2001.
# Lwandle Mgidlana <lwandle@translate.org.za>, 2002
#
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:51+0000\n"
"PO-Revision-Date: 2002-11-04 09:21SAST\n"
"Last-Translator: Lwandle Mgidlana <lwandle@translate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"Language: xh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0beta2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:36
#, kde-format
msgid "Pointer device KCM"
msgstr ""

#: kcm/libinput/libinput_config.cpp:38
#, kde-format
msgid "System Settings module for managing mice and trackballs."
msgstr ""

#: kcm/libinput/libinput_config.cpp:40
#, kde-format
msgid "Copyright 2018 Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43 kcm/xlib/xlib_config.cpp:91
#, kde-format
msgid "Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43
#, kde-format
msgid "Developer"
msgstr ""

#: kcm/libinput/libinput_config.cpp:109
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:114
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:125
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:145
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:167
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:191
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:193
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:79
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:103 kcm/libinput/main_deviceless.qml:54
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "&Ngokubanzi"

#: kcm/libinput/main.qml:105
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:124
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:130 kcm/libinput/main_deviceless.qml:56
#, fuzzy, kde-format
#| msgid "Le&ft handed"
msgid "Left handed mode"
msgstr "Ok&wesandla sasekunene"

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:75
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:155 kcm/libinput/main_deviceless.qml:81
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:174 kcm/libinput/main_deviceless.qml:100
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:184 kcm/libinput/main_deviceless.qml:110
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Pointer speed:"
msgstr "Isalathisi sendawo yokungena:"

#: kcm/libinput/main.qml:216 kcm/libinput/main_deviceless.qml:142
#, fuzzy, kde-format
#| msgid "Acceleration &profile:"
msgid "Acceleration profile:"
msgstr "&Unikezelo lwemboniselo yasecaleni:"

#: kcm/libinput/main.qml:247 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Flat"
msgstr ""

#: kcm/libinput/main.qml:250 kcm/libinput/main_deviceless.qml:176
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:257 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Adaptive"
msgstr ""

#: kcm/libinput/main.qml:260 kcm/libinput/main_deviceless.qml:186
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:272 kcm/libinput/main_deviceless.qml:198
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:274 kcm/libinput/main_deviceless.qml:200
#, kde-format
msgid "Invert scroll direction"
msgstr ""

#: kcm/libinput/main.qml:289 kcm/libinput/main_deviceless.qml:215
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:295
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Scrolling speed:"
msgstr "Isalathisi sendawo yokungena:"

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:349
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:360
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:396
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:427
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:431
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:448
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr ""

#: kcm/libinput/main.qml:449
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:478
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""
"<h1>Mouse</h1> Lo mnqongo womlinganiselo ukuvumela ukukhetha iinketho "
"ezahlukeneyo ngendlela apho icebo lokwalatha lisebenza khona. Icebo lakho "
"lokwalatha linokuba yi mouse, ibhola yophawu, okanye ezinye iintsimbi "
"ezisebenza umsebenzi ofanayo."

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "&Ngokubanzi"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""
"Ukuba usebenzisa isandla-sasekhohlo, ungakhetha ukutshintsha imisebenzi "
"yasekhohlo kunye namaqhiosha aekunene kwiceboi lakho lokwalatha ngokukhetha  "
"ukhetho 'lwesandla-sasekhohlo'. UKuba icebo lakho lokwalatha linamaqhosha "
"angapha kwesibini, ezo kuphela zisebenzayo njengamaqhosha asekhohlo "
"nasekunene echaphazeleka. Umzekelo, ukuba unemouse enamaqhosha-amathathu, "
"iqhosha eliphakathi alichaphazeleki."

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, fuzzy, kde-format
msgid "Righ&t handed"
msgstr "O&kwesandla sasekunene"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "Ok&wesandla sasekunene"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "Ukubheka phambili"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "Unikezelo lwesalathisi:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "Isalathisi sendawo yokungena:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "Nqakraza kabini kwithuba sokuphumla:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "Rhuqa ixesha lokuqala:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "Rhuqa uqalo lomgamo:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "Ivili le mouse lirolwa ngu:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, fuzzy, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""
"Olu khetho likuvumela ukuba utshintshe uzalano phakathi komgama oshukunyiswa "
"sisalathisi se mouse kwikhusi kunye nentshukumo efanayo yecebo "
"elizibonakalisa ngokwalo (elinokuba yi mouse, ibhola yomonakalo, okanye "
"elinye icebo lokwalatha.)<p> Ixabiso eliphezulu lokunikezela lizakusa "
"kwiintshukumo ezinkulu zesalathisi se mouse kwikhusi naxa usenza intshukumo "
"encinane ngecebo elibonakalayo. Ukukhetha amaxabiso aphezulu kunokubangela "
"isalathisi se mouse sibhabhe ngapha kwekhusi, iyenza nzima ukulawula!<p> "
"Ungacwangcisa ixabiso lonikezelo ngokutsala iqhosha lwesityibilikisi okanye "
"ngoku nqakraza phezulu/ezantsi iintolo ezikwiqhosha-lokujikeleza ekhohlo "
"kwesityibilikisi."

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, fuzzy, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""
"Ungeno ngumgama omncinane esimele isalathisi semouse sihambise ngawo ikhusi "
"phambi kokuba unikezelo lube nasonasiphi na isiphumo. Ukuba intshukumo "
"incinane kunongeno, isalathisi se mouse siyahamba ngokungathi unikezelo "
"belucwangcisiwe ku 1X.<p> Nje, xa usenza iintshukumo ezincinane ngecebo "
"elibonakalayo, akukho lunikezelo nakanye, ikunika iqondo elikhulu lolawulo "
"ngapha kwesalathisi se mouse. Ngeentshukumo ezinkulu zecebo elibonakalayo, "
"ungahambisa isalathisi se mouse ngokukhawuleza kwi ariya ezahlukileyo "
"kwikhusi.<p> Unga cwangcisa ixabiso longeno ngokutsala iqhosha "
"lwesityibilikisi okanye ngokunqakraza phezulu/ezantsi iintolo zeqhosha-"
"lokujikeleza ekhohlo kwesityibilikisi."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""
"Ithuba lokuphumla lokunqakraza kabini lixesha elininzi (kwimizuzwana "
"emininzi) phakathi konqakrazo kabini lwemouse oluzijikela kunqakrazo kabini. "
"Ukuba unqakrazo lwesibini luyenzeka mva kunelixesha lokuphumla emva "
"konqakrazo lokuqala, zijongwa njengonqakrazo kabini olwahlukeneyo."

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " msec"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""
"Ukuba unqakraza nge mouse (umzekelo umhleli welayini-ezininzi) kwaye uqale "
"ukususa imouse ngexesha lokuqala utsalo, umsebenzi wotsalo uzakwenziwa."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""
"Ukuba unqakraza ngemouse kwaye uqale ukususa imouse noko kumgama wokuqala "
"utsalo, umsebenzi wotsalo uzakwenziwa."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""
"Ukuba usebenzisa ivili le mouse,eli xabiso ligqiba inani le layini ukurola "
"nayiphi na intshukumo yevili ngalinye. Qaphela ukuba ukuba eli nani likhulu "
"kune nani lelayini ezibonakalayo, izakulahlwa kwaye intshukumo yevili "
"izakuphathwa njengentshukumo yephepha phezulu/ezantsi."

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, fuzzy, kde-format
#| msgid "Mouse Navigation"
msgid "Keyboard Navigation"
msgstr "Ulawulo lwe Mouse"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, fuzzy, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "&Shukumisa i mouse ngebhodi yezitshixo (usebenzisa i num pad)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "&Ulibaziso lonikezelo:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, fuzzy, kde-format
msgid "R&epeat interval:"
msgstr "&Phinda ithuba lokuphumla:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "&Ixesha lonikezelo:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, fuzzy, kde-format
msgid "Ma&ximum speed:"
msgstr "&Isantya esikhulu:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, fuzzy, kde-format
msgid " pixel/sec"
msgstr " pixels"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "&Unikezelo lwemboniselo yasecaleni:"

#: kcm/xlib/xlib_config.cpp:79
#, kde-format
msgid "Mouse"
msgstr ""

#: kcm/xlib/xlib_config.cpp:83
#, kde-format
msgid "(c) 1997 - 2018 Mouse developers"
msgstr ""

#: kcm/xlib/xlib_config.cpp:84
#, kde-format
msgid "Patrick Dowler"
msgstr ""

#: kcm/xlib/xlib_config.cpp:85
#, kde-format
msgid "Dirk A. Mueller"
msgstr ""

#: kcm/xlib/xlib_config.cpp:86
#, kde-format
msgid "David Faure"
msgstr ""

#: kcm/xlib/xlib_config.cpp:87
#, kde-format
msgid "Bernd Gehrmann"
msgstr ""

#: kcm/xlib/xlib_config.cpp:88
#, kde-format
msgid "Rik Hemsley"
msgstr ""

#: kcm/xlib/xlib_config.cpp:89
#, kde-format
msgid "Brad Hughes"
msgstr ""

#: kcm/xlib/xlib_config.cpp:90
#, kde-format
msgid "Brad Hards"
msgstr ""

#: kcm/xlib/xlib_config.cpp:283 kcm/xlib/xlib_config.cpp:288
#, fuzzy, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " pixels"
msgstr[1] " pixels"

#: kcm/xlib/xlib_config.cpp:293
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] ""
msgstr[1] ""

#, fuzzy
#~| msgid "Acceleration &time:"
#~ msgid "Acceleration:"
#~ msgstr "&Ixesha lonikezelo:"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration Profile:"
#~ msgstr "&Unikezelo lwemboniselo yasecaleni:"

#~ msgid "Icons"
#~ msgstr "Uphawu lwemifanekiso"

#~ msgid ""
#~ "The default behavior in KDE is to select and activate icons with a single "
#~ "click of the left button on your pointing device. This behavior is "
#~ "consistent with what you would expect when you click links in most web "
#~ "browsers. If you would prefer to select with a single click, and activate "
#~ "with a double click, check this option."
#~ msgstr ""
#~ "Ukuziphatha kokwendalo kwi KDE kukukhetha kunye nokusebenzisa ii icon "
#~ "ngonqakrazo olunye lweqhosha lasekhohlo kwicebo lakho lokwalatha. Oku "
#~ "kuziphatha kuyahluka nokulindeleyo xa unqakraza amakhonkco kubakhangeli "
#~ "abaninzi bencwadi be web. Ukuba ukhetha ukukhetha unqakrazo olunye, kunye "
#~ "nokusebenza ngonqakrazo kabini, khangela olu khetho."

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "Nqakr&aza-kabini ukuvula iifayile kunye neencwadi ezinenkcukacha (khetha "
#~ "ii icons kunqakrazo lokuqala)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr ""
#~ "Sebenzisa uvule ifayile okanye incwadi eneenkcukacha enonqakrazo olunye."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "&Nqakraza-kubekanye ukuvula iifayile kunye neencwadi ezinenkcukacha"

#, fuzzy
#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr ""
#~ "KDE kufuneka iqale kwakhona ngenxa yokutshintsha kobungakanani "
#~ "besalathisi ukwenza isiphumo"

#, fuzzy
#~ msgid "Large black cursors"
#~ msgstr "&Isalathisi esikhulu"

#, fuzzy
#~ msgid "Small white cursors"
#~ msgstr "&Isalathisi esimhlophe"

#, fuzzy
#~ msgid "Large white cursors"
#~ msgstr "&Isalathisi esikhulu"

#, fuzzy
#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "Tshintsha ubume besalathisi ngap&hezulu kwe icons"

#~ msgid "A&utomatically select icons"
#~ msgstr "K&hetha iimphawu ngokuzenzekelayo"

#, fuzzy
#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "Uliba&ziseko:"

#, fuzzy
#~| msgid " msec"
#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr " msec"

#~ msgid ""
#~ "If you check this option, pausing the mouse pointer over an icon on the "
#~ "screen will automatically select that icon. This may be useful when "
#~ "single clicks activate icons, and you want only to select the icon "
#~ "without activating it."
#~ msgstr ""
#~ "Ukuba ukhangela olu khetho, uphumlisa isalathisi se mouse ngapha kwe icon "
#~ "kwi khusi kuzakukhetha ngokuzenzekelayo lo icon. Oku kunokuba luncedo xa "
#~ "unqakrazo olunye lusebenzisa ii icon, kwaye ufuna ukukhetha i icon "
#~ "kuphela ngaphandle kokuyenza isebenze."

#~ msgid ""
#~ "If you have checked the option to automatically select icons, this slider "
#~ "allows you to select how long the mouse pointer must be paused over the "
#~ "icon before it is selected."
#~ msgstr ""
#~ "Ukuba ukhangele ukhetho ukukhetha ngokuzenzekelayo ii icon, esi "
#~ "sityibilikisi sikuvumela ukhethe ithuba isalathisi se mouse esizakuphumla "
#~ "ngapha kwe icon phambi kokuba ikhethwe."

#~ msgid "Short"
#~ msgstr "Mfutshane"

#~ msgid "Long"
#~ msgstr "Nde"

#~ msgid "Show feedback when clicking an icon"
#~ msgstr "Bonisa ingxelo xa unqakraza uphawu olunomfanekiso"

#, fuzzy
#~ msgid "Visual f&eedback on activation"
#~ msgstr "&Ingxelo ebonakalayo yokusebenza"
